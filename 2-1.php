<?php
$file = file_get_contents(__DIR__ . '/phone_book.json');
$data = json_decode($file, true);
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Телефонная книжка</title>
</head>

<body>
    <table>
        <thead>
            <tr>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>Адрес</th>
                <th>Номер телефона</th>
            </tr>
        </thead>
        <?php
        foreach ($data as $names) { ?>
            <tbody>
                <tr>
                    <td><?php echo $names["firstName"]?></td>
                    <td><?php echo $names["lastName"]?></td>
                    <td><?php echo $names["address"]?></td>
                    <td><?php echo $names["phoneNumber"]?></td>
                </tr>
            </tbody>
<?php } ?>
</table>
</body>
</html>